<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Kinkop66 - Index</title>

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-92423212-1', 'auto');
        ga('require', 'ec');


        ga('ec:addProduct', {               // Provide product details in a productFieldObject.
            'id': 'P12345',                   // Product ID (string).
            'name': 'Android Warhol T-Shirt', // Product name (string).
            'category': 'Apparel',            // Product category (string).
            'brand': 'Google',                // Product brand (string).
            'variant': 'Black',               // Product variant (string).
            'position': 1,                    // Product position (number).
            'dimension1': 'Member'            // Custom dimension (string).
        });
        ga('set', 'dimension1', 'ID1111');

        ga('ec:setAction', 'click', {       // click action.
            'list': 'Search Results'          // Product list (string).
        });


        ga('send', 'pageview');   // Pageview for payment.html

    </script>
    <!-- End Google Analytics -->
</head>
<body>

</body>
</html>